﻿using System;
using System.Collections.Generic;
using ADO.Interfaces;
using System.Data.SqlClient;
using ADO.Models;
using System.Data;

namespace ADO.Repositiories
{
    public class OrderRepository : IOrderRepository
    {
        private readonly string connectionString;

        private const string INSERT = "INSERT INTO [dbo].[Order] (Status, CreatedDate, UpdateDate, ProductId) " +
                                      "VALUES (@Status, @CreatedDate, @UpdateDate, @ProductId)";
        private const string SELECT = "SELECT Id, Status, CreatedDate, UpdateDate, ProductId FROM [dbo].[Order] " +
                                      "WHERE Id = @Id";
        private const string UPDATE = "UPDATE [dbo].[Order] " +
                                      "SET Status = @Status, " +
                                      "CreatedDate = @CreatedDate, " +
                                      "UpdateDate = @UpdateDate " +
                                      "WHERE Id = @Id";
        private const string DELETE = "DELETE FROM [dbo].[Order] WHERE Id = @Id";

        private const string ProcedureDELETE = "DeleteOrders";
        private const string ProcedureFETCH = "FetchOrders";

        public OrderRepository(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public void Create(Order entity)
        {
            using var connection = new SqlConnection(connectionString);
            connection.Open();

            var command = new SqlCommand(INSERT, connection);

            command.Parameters.AddWithValue("@Status", entity.Status);
            command.Parameters.AddWithValue("@CreatedDate", entity.CreatedDate);
            command.Parameters.AddWithValue("@UpdateDate", entity.UpdatedDate);
            command.Parameters.AddWithValue("@ProductId", entity.ProductId);

            command.ExecuteNonQuery();
        }

        public Order Read(int id)
        {
            Order order = null;
            using var connection = new SqlConnection(connectionString);

            connection.Open();

            var command = new SqlCommand(SELECT, connection);
            command.Parameters.AddWithValue("@Id", id);
            using var reader = command.ExecuteReader();

            if (!reader.HasRows) return null;

            while (reader.Read())
            {
                order = new Order
                {
                    Id = reader.GetInt32(0),
                    Status = (OrderStatus)reader.GetInt32(1),
                    CreatedDate = reader.GetDateTime(2),
                    UpdatedDate = reader.GetDateTime(3),
                    ProductId = reader.GetInt32(4)
                };
            }

            return order;
        }

        public void Update(Order entity, int id)
        {
            using var connection = new SqlConnection(connectionString);
            connection.Open();

            var command = new SqlCommand(UPDATE, connection);

            command.Parameters.AddWithValue("@Id", id);
            command.Parameters.AddWithValue("@Status", entity.Status);
            command.Parameters.AddWithValue("@CreatedDate", entity.CreatedDate);
            command.Parameters.AddWithValue("@UpdateDate", entity.UpdatedDate);

            command.ExecuteNonQuery();
        }

        public void Delete(int id)
        {
            using var connection = new SqlConnection(connectionString);
            connection.Open();

            var command = new SqlCommand(DELETE, connection);
            command.Parameters.AddWithValue("@Id", id);

            command.ExecuteNonQuery();
        }

        public void Delete(int? month = null, OrderStatus? status = null, int? year = null, int? productId = null)
        {

            using var connection = new SqlConnection(connectionString);
            connection.Open();

            var sqlTransaction = connection.BeginTransaction();
            var command = new SqlCommand(ProcedureDELETE, connection);
            command.Transaction = sqlTransaction;
            command.CommandType = CommandType.StoredProcedure;

            command.Parameters.AddWithValue("@Month", month);
            command.Parameters.AddWithValue("@Status", status);
            command.Parameters.AddWithValue("@Year", year);
            command.Parameters.AddWithValue("@ProductId", productId);

            try
            {
                command.ExecuteScalar();
                sqlTransaction.Commit();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                sqlTransaction.Rollback();
                throw;
            }
        }

        public IEnumerable<Order> GetAll(int? month = null, OrderStatus? status = null, int? year = null, int? productId = null)
        {
            var orders = new List<Order>();

            using var connection = new SqlConnection(connectionString);
            connection.Open();
            var command = new SqlCommand(ProcedureFETCH, connection);

            command.CommandType = CommandType.StoredProcedure;

            command.Parameters.AddWithValue("@Month", month);
            command.Parameters.AddWithValue("@Status", status);
            command.Parameters.AddWithValue("@Year", year);
            command.Parameters.AddWithValue("@ProductId", productId);

            using var reader = command.ExecuteReader();

            if (!reader.HasRows) return null;

            while (reader.Read())
            {
                var order = new Order
                {
                    Id = reader.GetInt32(0),
                    Status = (OrderStatus)reader.GetInt32(1),
                    CreatedDate = reader.GetDateTime(2),
                    UpdatedDate = reader.GetDateTime(3),
                    ProductId = reader.GetInt32(4)
                };
                orders.Add(order);
            }

            return orders;
        }
    }
}
