﻿using ADO.Models;

namespace ADO.Interfaces
{
    public interface IProductRepository : IRepository<Product>
    {
        IEnumerable<Product> GetAll();
    }
}
