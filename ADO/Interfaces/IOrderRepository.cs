﻿using ADO.Models;

namespace ADO.Interfaces
{
    public interface IOrderRepository : IRepository<Order>
    {
        void Delete(
            int? month = null,
            OrderStatus? status = null,
            int? year = null,
            int? productId = null);

        IEnumerable<Order> GetAll(
            int? month = null,
            OrderStatus? status = null,
            int? year = null,
            int? productId = null);
    }
}
